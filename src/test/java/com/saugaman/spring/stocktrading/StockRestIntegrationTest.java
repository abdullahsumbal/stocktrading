package com.saugaman.spring.stocktrading;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.saugaman.spring.stocktrading.data.TradeMongoDao;
import com.saugaman.spring.stocktrading.entities.Trade;
import com.saugaman.spring.stocktrading.rest.TradeRest;
import com.saugaman.spring.stocktrading.service.TradeServiceImpl;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;



@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes=StockRestIntegrationTest.Config.class)
public class StockRestIntegrationTest {

    public static final String TEST_ID = "5f46a3d545bee629d17fd7b2";

    @Configuration
    public static class Config {

        // We really shouldn't need this, but when Spring tries to configure a
        // CompactDiscService bean -- even a mock one! it insists on resolving
        // the @Autowired dependency to a CompactDiscRepository.
        @Bean
        public TradeMongoDao repo() {
            return mock(TradeMongoDao.class);
        }

        @Bean
        public TradeServiceImpl service() {

            ObjectId ID = new ObjectId(TEST_ID);
            Trade stock = new Trade();
            List<Trade> stocks = new ArrayList<>();
            stocks.add(stock);

            TradeServiceImpl service = mock(TradeServiceImpl.class);
            when(service.getStock(ID)).thenReturn(Optional.of(stock));
            when(service.getStocks()).thenReturn(stocks);
            return service;
        }

        @Bean
        public TradeRest controller() {
            return new TradeRest();
        }
    }

    private TradeRest controller;

    @BeforeEach
    void setup(@Autowired TradeRest controller){
        this.controller = controller;
    }
    
    @Test
    public void testFindAll() {
        Iterable<Trade> stocks = controller.findAll();
        Stream<Trade> stream = StreamSupport.stream(stocks.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }
    
    @Test
    public void testStockById() {
        Optional<Trade> stock = controller.findByID(TEST_ID);
        assertThat(stock.isPresent(), equalTo(true));
    }

    
}