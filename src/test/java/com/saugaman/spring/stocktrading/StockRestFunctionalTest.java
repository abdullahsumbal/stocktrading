package com.saugaman.spring.stocktrading;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.saugaman.spring.stocktrading.entities.TradeState; 

import java.util.List;

import com.saugaman.spring.stocktrading.entities.Trade;

import org.springframework.web.client.RestTemplate;

public class StockRestFunctionalTest {

    private RestTemplate template = new RestTemplate();

    @Test
    public void testFindAll() {
        @SuppressWarnings("unchecked") // specifying raw List.class
        List<Trade> stocks = template.getForObject("http://localhost:8080/stock", List.class);

        assertThat(stocks, hasSize(1));
    }
    
    @Test
    public void testStockById() {
        Trade stock = template.getForObject("http://localhost:8080/stock/5f4d13c03682fc755e35ff58", Trade.class);
        assertThat(stock, notNullValue());
    }

    @Test
    public void testStockStatus() {
        template.put("http://localhost:8080/stock/5f4d13c03682fc755e35ff58/PENDING", null);

        Trade stock = template.getForObject("http://localhost:8080/stock/5f4d13c03682fc755e35ff58", Trade.class);
        assertEquals(stock.getState(), TradeState.REJECTED);
    }
    
}