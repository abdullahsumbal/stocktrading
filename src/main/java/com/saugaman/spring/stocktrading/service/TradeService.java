
package com.saugaman.spring.stocktrading.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.saugaman.spring.stocktrading.entities.TradeState;
import com.saugaman.spring.stocktrading.entities.Holding;
import com.saugaman.spring.stocktrading.entities.Trade;

import org.bson.types.ObjectId;


public interface TradeService {
    
    Collection<Trade> getStocks();
    Optional<Trade> getStock(ObjectId id);
    ObjectId addStock(Trade stock);
    void updateStatus(ObjectId id,TradeState status);
    List<Trade> findByState(TradeState state);
    List<Holding> getHoldings();


}