package com.saugaman.spring.stocktrading.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.saugaman.spring.stocktrading.data.HoldingMongoDao;
import com.saugaman.spring.stocktrading.data.TradeMongoDao;
import com.saugaman.spring.stocktrading.entities.TradeState;
import com.saugaman.spring.stocktrading.entities.Holding;
import com.saugaman.spring.stocktrading.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeMongoDao repo;

    @Autowired
    private HoldingMongoDao repoHolding;

    @Override
    public Collection<Trade> getStocks() {
        // TODO Auto-generated method stub
        return repo.findAll();
    }

    @Override
    public ObjectId addStock(Trade stock) {
        // TODO Auto-generated method stub
        return repo.insert(stock).getId();
    }

    @Override
    public void updateStatus(ObjectId id, TradeState status) {
        Optional<Trade> stock = repo.findById(id);
        if (stock.isPresent()) {
            Trade stockObject = stock.get();
            if (stockObject.getState().equals(TradeState.CREATED)) {
                stockObject.setState(status);
                repo.save(stockObject);
            }
        }
    }

    @Override
    public Optional<Trade> getStock(ObjectId id) {
        return repo.findById(id);

    }

    @Override
    public List<Trade> findByState(TradeState state) {
        return repo.findByState(state);
    }

    @Override
    public List<Holding> getHoldings() {
        return repoHolding.findAll() ;
    }
}