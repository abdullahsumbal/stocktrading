package com.saugaman.spring.stocktrading.rest;

import java.util.List;
import java.util.Optional;

import com.saugaman.spring.stocktrading.entities.TradeState;
import com.saugaman.spring.stocktrading.entities.Holding;
import com.saugaman.spring.stocktrading.entities.Trade;
import com.saugaman.spring.stocktrading.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stock")
@CrossOrigin
public class TradeRest {

	@Autowired
	private TradeService service;

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Trade> findAll() {
		// logger.info("managed to call a Get request for findAll");
		return service.getStocks();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public Optional<Trade> findByID(@PathVariable("id") String id) {
		// logger.info("managed to call a Get request for findAll");
		return service.getStock(new ObjectId(id));
    }
    
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.CREATED)
	@ResponseBody
	public String addStock(@RequestBody Trade stock) {
		return service.addStock(stock).toString();
    }
    
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/{status}")
	public void deleteCd(@PathVariable("id") String id, @PathVariable("status") TradeState status) {
		service.updateStatus(new ObjectId(id), status);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/state/{status}")
	public List<Trade> findByState(@PathVariable("status") TradeState status) {
		return service.findByState(TradeState.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/holding")
	public List<Holding> getHoldings() {
		return service.getHoldings();
    }
	

}