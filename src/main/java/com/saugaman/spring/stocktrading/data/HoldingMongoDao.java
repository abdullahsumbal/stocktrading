package com.saugaman.spring.stocktrading.data;

import java.util.List;

import com.saugaman.spring.stocktrading.entities.Holding;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HoldingMongoDao extends MongoRepository<Holding, ObjectId> {

	public List<Holding> findByTicker(String ticker);
}