package com.saugaman.spring.stocktrading.data;

import java.util.List;

import com.saugaman.spring.stocktrading.entities.Trade;
import com.saugaman.spring.stocktrading.entities.TradeState;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeMongoDao extends MongoRepository<Trade, ObjectId> {
    public List<Trade> findByState(TradeState state);
}