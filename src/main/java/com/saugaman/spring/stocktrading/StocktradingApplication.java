package com.saugaman.spring.stocktrading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackages = "com.saugaman.spring.stocktrading")
public class StocktradingApplication {

	public static void main(String[] args) {
		SpringApplication.run(StocktradingApplication.class, args);
	}

}
